// Set default margins, width and height
var margin = {left: 100, top: 10, right: 10, bottom: 100}
var width = 800 - margin.left - margin.right
var height = 600 - margin.top - margin.bottom

// Add SVG to bar chart container
var svg = d3.select('#bar-chart')
			.append('svg')
				.attr('width', width + margin.left + margin.right)
				.attr('height', height + margin.top + margin.bottom)

// Add Group element to the SVG and translate it to center
var g = svg.append('g')
			.attr('transform', 'translate(' + margin.left + ', ' + margin.top + ')')

// Add X-label to Bar Chart
g.append('text')
	.attr('class', 'x-axis-label')
	.attr('x', width/2)
	.attr('y', height + 95)
	.attr('font-size', '20px')
	.attr('text-anchor', 'middle')
	.text('Language')

// Add Y-label to Bar Chart
g.append('text')
	.attr('class', 'y-axis-label')
	.attr('x', -height/2)
	.attr('y', -60)
	.attr('font-size', '20px')
	.attr('text-anchor', 'middle')
	.attr('transform', 'rotate(-90)')
	.text('Artifacts Count')


// Make all count values in the data integers from string(default)
data.forEach(function(data) {
	data.count = +data.count
}) 

// Create Band and Linear scales (to scale X and Y-axes values accordingly)
var x = d3.scaleBand()
	.domain(data.map(function(d) {
		return d.language
	}))
	.range([0, width])
	.paddingOuter(0.3)
	.paddingInner(0.3)
	
var y = d3.scaleLinear()
	.domain([0, d3.max(data, function(d) {
		return d.count
	})])
	.range([height, 0])
	
	
// Add X and Y-axes to the Bar Chart
var xAxis = d3.axisBottom(x)
g.append('g')
	.attr('class', 'x-axis')
	.attr('transform', 'translate(0, ' + height + ')')
	.call(xAxis)
    .selectAll('text')
		.attr('y', 10)
		.attr('x', -5)
		.attr('text-anchor', 'end')
		.attr('transform', 'rotate(-30)');
	
var yAxis = d3.axisLeft(y)
g.append('g')
	.attr('class', 'y-axis')
	.call(yAxis)

	
// Add data and display the Bar chart
var chart = g.selectAll('rect')
	.data(data)
	.enter()
	.append('rect')
		.attr('x', function(d) {
            return x(d.language)
        })
		.attr('y', function(d) {
            return y(d.count) 
        })
		.attr('width', x.bandwidth())
		.attr('height', function(d) {
            return height - y(d.count)
        })
		.attr('fill', 'gray')
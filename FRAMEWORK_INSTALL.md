# Installation on Linux

The installation process has been according to Ubuntu 16.04

0. On Ubuntu, confirm that the prerequisites are installed:

    - The system should have `python3` installed in it.

    - Install `virtualenv` and `git`
        ```
        sudo apt install virtualenv git
        ```
    - Install docker by following the instructions on https://docs.docker.com/v17.12/install/.

    Also note that on Linux, docker commands usually need to be prefixed with `sudo`.
    So, if you see any message saying _permission denied_ or something like that, then try running that using `sudo`.
    To avoid needing `sudo` for docker commands, add your username to the docker group:
        ```
        sudo usermod -aG docker $USER
        ```


1. Create a virtual environment in python to install various dependency packages
	```
	virtualenv cdli_env --python=python3
	```
	We are naming the virtual environment as `cdli_env`.

2. Shift to the virtual environment created
	```
	source cdli_env/bin/activate
	```
    After this, you would be able to see `(cdli_env)` in the beginning of your terminal prompt.
    This shows that the virtual environment has been activated.

3. Clone the framework repository
	```
	git clone https://gitlab.com/cdli/framework
	```
	Now you will be asked to fill in your gitlab login credentials.

    Now if you want to clone the submodules also:-
    - Shift to the framework directory
        ```
        cd framework 
        ```
    - Initialize your local configuration file
        ```
        git submodule init
        ```
    - Fetch all the data from that submodules
        ```
        git submodule update
        ```

5. Install docker-compose in the python virtual-env
	```
	pip install docker-compose
	```
	This will install `docker-compose` package and various other packages on which this package depends on.

6. Login to docker image of `orchestrator/manifests`
    ```
    docker login https://registry.gitlab.com/v2/cdli/framework/orchestrator/manifests/0.0.3
    ```
    Now you will be asked to fill in your login credentials.
	
	**OR**
	
	```
	docker login https://registry.gitlab.com/
	```
   You can use your gitlab username and password here, or visit https://gitlab.com/profile/personal_access_tokens and create a new access token with the `read_registry` scope and use that for the password instead.

7. If you are on Linux run the following commands
	```
	setfacl -R -m default:user:2354:rwX,user:2354:rwX [framework_folder]
	```
   This command which sets up extended ACL permissions on the folder without requiring any changes to its owner, group, or mode.

8. Run the framework by the following command
	```
	./dev/cdlidev.py up
	```
	
9. If it runs fine, you will be having the following services running:
	- Nginx, available to the host at http://127.0.0.1:2354
	- phpMyAdmin, available to the host at http://127.0.0.1:2355
	- MariaDB, available internally to containers at tcp://mariadb:3306
	- Redis, available internally to containers at tcp://redis:6379

10. You can also, have a look at the docker containers by running
	```
	docker ps
	```
	#### Docker Container Access Commands
	- Docker compose names containers following the scheme `cdlidev_[app_][name]_1`.
	- For example, MariaDB will be named `cdlidev_mariadb_1` and the search app named `cdlidev_app_search_1`.
	- To start a live shell in a container: `docker exec -ti [container_name] sh`
	- To copy a file to a container's `/tmp` folder: `docker cp [local_path] [container_name]:[container_path]`

11. Just to confirm that everything is fine till here
	- Open the link, http://127.0.0.1:2354 and you will see some login page. If you are able to see it, then you are going on fine.
	- Also, open http://127.0.0.1:2355, and you will see the _phpMyAdmin_ login page. The credentials are the default one i.e.
        - _username_: 'root'
        - _password_: ''

12. Get the latest `.sql` file of the database. Now we assume the filename to be `cdli_db.sql`.

13. Move the database to the docker container by running the following command:-
	```
	docker cp cdli_db.sql cdlidev_mariadb_1:/tmp/cdli_db.sql
	```

14. Now open the MariaDB instance to upload the database.
    We will not be uploading using _phpMyAdmin_ because the file size is greater than the size limit provided by it.
    Open the docker container of MariaDB by running the following command:-
    ```
    docker exec -it cdlidev_mariadb_1 sh
    ```

15. Now in order to upload the database, we have to create an empty database in MySQL. To do that,
	- Open MySQL. For this, you won't be needing any username or password since it is set to default.
	    ```
        mysql
        ```
        This will lead us to the MySQL shell.
	- Create a database in MySQL, by running the following command:-
        ```
        MariaDB [(none)]> create database cdli_db;
        ```
	- Leave the MySQL, by running
        ```
        MariaDB [(none)]> exit
        ```
    Here `MariaDB [(none)]>` is a prompt from our MySQL shell.

16. Now you would be back on your terminal of the MariaDB docker container. Now upload the `/tmp/cdli_db.sql` of the container to the MySQL database.
    For that run the following command:
    ```
    mysql cdli_db < /tmp/cdli_db.sql
    ```
    Since it is a large database it will take some time to upload. On one instance, it took 30 minutes.

17. You can have a look at the credentials of the database [here](https://gitlab.com/cdli/framework/blob/phoenix/develop/app/cake/config/app.php#L251).

19. Now your website would be up and available at http://127.0.0.1:2354

20. Registration
	- Access http://localhost:2354/register
	- Input *username*,  *password* and *email*.
	- Click **Sign Up**.
	- Use  **Google Authenticator** to scan the QR code, input the *code*, back up the code and check the checkbox.
	- Click **Enable 2FA**.

21. Log in
	- Access any webpage.
	- Input *username*, *password*.
	- Click **Login**.
	- After redicted to the 2FA page, input the *code*.
	- Click  **Login**.

22. In order to search:-
	- Open the http://127.0.0.1:2354/search/ page
	- Just in order to check as an example, in the dropdown, select `CDLI No.` option off and enter the number `111946` in the search bar. You'll see some results.


# Installation on Windows

The installation process has been tested on Windows 10.

0. Prerequisites: **Git for Windows**, **Python for Windows**. Open PowerShell or Command Prompt and check if Python and Git are installed by running the following commands. If the commands are not found, install [Git](https://git-scm.com/downloads) or [Python](https://www.python.org/downloads/windows/) from their official websites.
    ```
    git --version
    python --version
    ```

1. Download **Docker Desktop for Windows** Installer from the official website. A free account on Docker would be required before the download. Following errors might occur when Docker tries to start after the installation is complete:

    - **Error: Hyper-V is not enabled**:
    Docker runs virtualized environments right from hardware and thus requires Hyper-V. Press <kbd>Win+R</kbd> and launch `msinfo32`. Enter "BIOS" in the *Find* text box to see if your PC has UEFI or BIOS. Steps might differ slightly on different machines.
        - PC with UEFI:
            - Press Restart while holding Shift down.
            - Goto TroubleShoot > Advanced Options > UEFI Firmware Settings > Restart. 
            - Press <kbd>F10</kbd> > Goto System Configuration Tab using Arrow keys > Enable Virtualization Technology. Press <kbd>F10</kbd> to save and Exit.
        - PC with Legacy BIOS: 
            - Restart PC but press <kbd>Esc</kbd> as soon as anything appears on the screen. BIOS settings screen should be visible.
            - Press <kbd>F10</kbd> > Goto System Configuration Tab using Arrow keys > Enable Virtualization Technology. Press <kbd>F10</kbd> to save and Exit.
    
    - **Error: Docker does not have enough memory**: Right-click on the Docker icon in the notification area. Select Settings and goto the Advanced tab to specify the memory Docker should use. Docker runs smoothly for our purposes even with minimum memory.


2. Clone the framework repository and enter GitLab credentials when asked. Use Windows PowerShell or Command Prompt.
    ```   
    git clone https://gitlab.com/cdli/framework.git
    ```

3. Mounting drive: The drive in which the framework is located needs to be shared with Docker containers. If, for example, the framework is cloned into a folder in `C:`, then right click on the Docker icon and go to Settings > Shared Drives tab > Check `C:` > Apply. The following error might occur.
    - **Error: Firewall blocked file sharing between Windows and containers**: Open your Firewall program and add port 445 (Microsoft Directory Server Port) to the list of ports allowed to system services. 

4. Get `docker-compose` library for Python using the following command, similar to the Step 4 in Linux installation.
    ```
    pip install docker-compose
    ```

5. Login to docker image of `orchestrator/manifests`,  similar to Step 5 in Linux installation.
	```
	docker login https://registry.gitlab.com/
	```

6. Start the servers in Docker containers by either using the `docker compose` command or by running the script available in `dev` folder to do the same.
    ```
    python .\dev\cdlidev up
    ```

7. Continue from Step 7 of the Linux installation while executing the instructions on a new window of PowerShell or Command Prompt.



## Potentials Errors

The following errors might show up while starting the server using `docker-compose` or the provided Python script. 

1. **Cannot start service mariadb:** Driver failed programming external connectivity on endpoint cdlidev_appname_1 <br>
Restart docker

2. **Cannot create container for service app_name:** Mount denied: The source path pathTo/framework/app/upload doesn't exist and is not known to Docker. <br>
    Make an empty directory named `upload` inside `app` directory.

The following errors may appear in place of the website:

3. **Error: Could not load configuration file: /srv/app/cake/config/app.php**: <br>
    Copy the file `app.default.php` to `app.php`.
    ```
    cp .\app\cake\config\app.default.php .\app\cake\config\app.php
    ```

4. **Error: SQLSTATE[HY000] [2002] No such file or directory**: <br>
    Open `app.php` and in the group of settings for `'Datasources'`, ensure that the following settings have the correct value:
    ```
    `host' => 'mariadb',
    'port' => '3306',

    //Use as logged-in during the Step 10 of Linux installation
    'username' => 'root',   
    'password' => '',

    //Use as named in Step 14 of Linux installation
    'database' => 'cdli_db',
    ```

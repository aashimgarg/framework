<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PostingTypes Controller
 *
 * @property \App\Model\Table\PostingTypesTable $PostingTypes
 *
 * @method \App\Model\Entity\PostingType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostingTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $postingTypes = $this->paginate($this->PostingTypes);

        $this->set(compact('postingTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Posting Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $postingType = $this->PostingTypes->get($id, [
            'contain' => ['Postings']
        ]);

        $this->set('postingType', $postingType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $postingType = $this->PostingTypes->newEntity();
        if ($this->request->is('post')) {
            $postingType = $this->PostingTypes->patchEntity($postingType, $this->request->getData());
            if ($this->PostingTypes->save($postingType)) {
                $this->Flash->success(__('The posting type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The posting type could not be saved. Please, try again.'));
        }
        $this->set(compact('postingType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Posting Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $postingType = $this->PostingTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $postingType = $this->PostingTypes->patchEntity($postingType, $this->request->getData());
            if ($this->PostingTypes->save($postingType)) {
                $this->Flash->success(__('The posting type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The posting type could not be saved. Please, try again.'));
        }
        $this->set(compact('postingType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Posting Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $postingType = $this->PostingTypes->get($id);
        if ($this->PostingTypes->delete($postingType)) {
            $this->Flash->success(__('The posting type has been deleted.'));
        } else {
            $this->Flash->error(__('The posting type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

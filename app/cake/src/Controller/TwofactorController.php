<?php
namespace App\Controller;

use App\Controller\AppController;
use GoogleAuthenticator\GoogleAuthenticator;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TwofactorController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Authors', 'Credits', 'Inscriptions']
        ]);

        $this->set('user', $user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadComponent('GoogleAuthenticator');
        $ga = $this->GoogleAuthenticator;

        $this->loadModel('Users');
        $user_cur_id = $this->Auth->user('id'); // Put your user id here
        $getcomp_user = $this->Users->get($user_cur_id);
        $userstatus = $getcomp_user['2fa_status'];
        $userkey = $getcomp_user['2fa_key'];

        if($userstatus == 1 && $userkey != "") {
            $this->redirect(["controller" => "Users", "action" => "index"]);
        }

        if($this->request->is('post'))
        {
            $checkconfirm = $this->request->data['checkconfirm'];
            if($checkconfirm == 0) {
                $this->Flash->error(__('Please back up your 16-digit key before proceeding.'), ['class' => 'alert alert-danger']);
                $this->redirect(array("controller" => "Twofactor","action" => "index"));
            }

            $secret = $this->request->data['secretcode'];
            $oneCode = $this->request->data['code'];
            $checkResult = $ga->verifyCode($secret, $oneCode, 2);    // 2 = 2*30sec clock tolerance

            if ($checkResult) {
                $savedata['2fa_key'] =  $this->request->data['secretcode'];
                $savedata['2fa_status'] = 1;
                $curuser = $this->Users->get($user_cur_id);
                $userupdate = $this->Users->patchEntity($curuser,$savedata);

                if ($this->Users->save($userupdate)) {
                    $this->Flash->success(__('Two-Factor Authentication (2FA) Is Enabled.'),array('class' => 'alert alert-danger'));
                    $this->redirect(array("controller" => "Search","action" => "index"));
                } else {
                    $this->Flash->error(__('Please try again.'),array('class' => 'alert alert-danger'));
                }
                
            } else {
                $this->Flash->error(__('Wrong code entered.Please try again.'),array('class' => 'alert alert-danger'));
            }
        }
    }
}

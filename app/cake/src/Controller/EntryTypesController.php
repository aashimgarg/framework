<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EntryTypes Controller
 *
 * @property \App\Model\Table\EntryTypesTable $EntryTypes
 *
 * @method \App\Model\Entity\EntryType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EntryTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $entryTypes = $this->paginate($this->EntryTypes);

        $this->set(compact('entryTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Entry Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $entryType = $this->EntryTypes->get($id, [
            'contain' => ['Publications']
        ]);

        $this->set('entryType', $entryType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $entryType = $this->EntryTypes->newEntity();
        if ($this->request->is('post')) {
            $entryType = $this->EntryTypes->patchEntity($entryType, $this->request->getData());
            if ($this->EntryTypes->save($entryType)) {
                $this->Flash->success(__('The entry type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The entry type could not be saved. Please, try again.'));
        }
        $this->set(compact('entryType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Entry Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $entryType = $this->EntryTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $entryType = $this->EntryTypes->patchEntity($entryType, $this->request->getData());
            if ($this->EntryTypes->save($entryType)) {
                $this->Flash->success(__('The entry type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The entry type could not be saved. Please, try again.'));
        }
        $this->set(compact('entryType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Entry Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $entryType = $this->EntryTypes->get($id);
        if ($this->EntryTypes->delete($entryType)) {
            $this->Flash->success(__('The entry type has been deleted.'));
        } else {
            $this->Flash->error(__('The entry type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

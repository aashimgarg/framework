<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Archive Entity
 *
 * @property int $id
 * @property string|null $archive
 * @property int|null $provenience_id
 *
 * @property \App\Model\Entity\Provenience $provenience
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Archive extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'archive' => true,
        'provenience_id' => true,
        'provenience' => true,
        'artifacts' => true
    ];
}

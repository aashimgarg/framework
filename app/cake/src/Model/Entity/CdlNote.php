<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CdlNote Entity
 *
 * @property int $id
 * @property string|null $year
 * @property int|null $number
 * @property int|null $author_id
 * @property string|null $title
 * @property string|null $text
 * @property string|null $footnotes
 * @property string|null $bibliography
 * @property \Cake\I18n\FrozenDate|null $uploaded
 * @property \Cake\I18n\FrozenDate|null $preprint
 * @property \Cake\I18n\FrozenDate|null $archival
 * @property string|null $preprint_state
 * @property string|null $online
 *
 * @property \App\Model\Entity\Author $author
 */
class CdlNote extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'year' => true,
        'number' => true,
        'author_id' => true,
        'title' => true,
        'text' => true,
        'footnotes' => true,
        'bibliography' => true,
        'uploaded' => true,
        'preprint' => true,
        'archival' => true,
        'preprint_state' => true,
        'online' => true,
        'author' => true
    ];
}

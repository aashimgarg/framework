<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RetiredArtifact $retiredArtifact
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Retired Artifact') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $retiredArtifact->has('artifact') ? $this->Html->link($retiredArtifact->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $retiredArtifact->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('New Artifact Id') ?></th>
                    <td><?= h($retiredArtifact->new_artifact_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($retiredArtifact->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Public') ?></th>
                    <td><?= $retiredArtifact->is_public ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Artifact Remarks') ?></th>
                    <td><?= $this->Text->autoParagraph(h($retiredArtifact->artifact_remarks)); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Retired Artifact'), ['action' => 'edit', $retiredArtifact->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Retired Artifact'), ['action' => 'delete', $retiredArtifact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $retiredArtifact->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Retired Artifacts'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Retired Artifact'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>




<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsCredit $artifactsCredit
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Artifacts Credits'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Credits'), ['controller' => 'Credits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Credit'), ['controller' => 'Credits', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="artifactsCredits form large-9 medium-8 columns content">
    <?= $this->Form->create($artifactsCredit) ?>
    <fieldset>
        <legend><?= __('Add Artifacts Credit') ?></legend>
        <?php
            echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
            echo $this->Form->control('credit_id', ['options' => $credits, 'empty' => true]);
            echo $this->Form->control('date', ['empty' => true]);
            echo $this->Form->control('credit_type');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>


<div>
	<?= __('Search')?>
	<?= $this->Form->create("",['type'=>'get']) ?>
	<?= $this->Form->control('Publications'); ?>
	<?= $this->Form->control('Collections'); ?>
	<?= $this->Form->control('Proveniences'); ?>
	<?= $this->Form->control('Periods'); ?>
	<?= $this->Form->control('Transliteraton'); ?>
	<?= $this->Form->control('CDLI'); ?>
	<?= $this->Form->button('Search', ['type' => 'submit']); ?>
	<?= $this->Form->end()?>
</div>
<?php if(!empty($result)){?>
	<?php foreach ($result as $row) {?>

		<table>
			<tr>
				<th rowspan="10"> 
					<?php echo $row["id"];?>
				</th>
				<th > 
					<?php echo $row["_matchingData"]["Publications"]["designation"];?>
				</th>
			</tr>
			<tr>
				<td >
					<?php echo "Author: ";?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Publications Date: ".$row["_matchingData"]["Publications"]["year"]; ?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "CDLI No.: ".$row["id"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Collection: ".$row["_matchingData"]["Collections"]["collection"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Provenience: ".$row["_matchingData"]["Proveniences"]["provenience"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Period: ".$row["_matchingData"]["Periods"]["period"];?>
				</td>
			</tr>		
			<tr>
				<td >
					<?php echo "Object Type: ".$row["_matchingData"]["ArtifactTypes"]["artifact_type"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Material: ".$row["_matchingData"]["Materials"]["material"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Transliteration/Translation: ".$row["_matchingData"]["Inscriptions"]["transliteration"];?>
				</td>
			</tr>
		</table>

	<?php }?>
<?php }?>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AuthorsPublication $authorsPublication
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($authorsPublication) ?>
            <legend class="capital-heading"><?= __('Add Authors Publication') ?></legend>
            <?php
                echo $this->Form->control('publication_id', ['options' => $publications, 'empty' => true]);
                echo $this->Form->control('author_id', ['options' => $authors, 'empty' => true]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Authors Publications'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>

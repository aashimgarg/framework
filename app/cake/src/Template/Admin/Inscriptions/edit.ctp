<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($inscription) ?>
            <legend class="capital-heading"><?= __('Edit Inscription') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('transliteration');
                echo $this->Form->control('transliteration_clean');
                echo $this->Form->control('tranliteration_sign_names');
                echo $this->Form->control('created_by');
                echo $this->Form->control('credit_to');
                echo $this->Form->control('is_latest');
                echo $this->Form->control('annotation');
                echo $this->Form->control('atf2conll_diff_resolved');
                echo $this->Form->control('atf2conll_diff_unresolved');
                echo $this->Form->control('comments');
                echo $this->Form->control('structure');
                echo $this->Form->control('translations');
                echo $this->Form->control('transcriptions');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $inscription->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $inscription->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Inscriptions'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsDateReferenced $artifactsDateReferenced
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Artifacts Date Referenced'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Rulers'), ['controller' => 'Rulers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ruler'), ['controller' => 'Rulers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Months'), ['controller' => 'Months', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Month'), ['controller' => 'Months', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="artifactsDateReferenced form large-9 medium-8 columns content">
    <?= $this->Form->create($artifactsDateReferenced) ?>
    <fieldset>
        <legend><?= __('Add Artifacts Date Referenced') ?></legend>
        <?php
            echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
            echo $this->Form->control('ruler_id', ['options' => $rulers, 'empty' => true]);
            echo $this->Form->control('month_id', ['options' => $months, 'empty' => true]);
            echo $this->Form->control('month_no');
            echo $this->Form->control('year_id');
            echo $this->Form->control('day_no');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

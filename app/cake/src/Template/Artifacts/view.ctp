<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */
?>
<span id="back">Back to Search Results</span>
<h1><?= h($artifact->designation) ?> (<?= h($artifact->id) ?>)</h1>
<h2>{Genre}, {Object type}, {Provenience} in {Period} and kept at {Museum Collection}</h2>

if image :
<div><div>
img src
View full image</div>
Line art
detail image
Lift RTI surface
pdf
</div>


<div id="artifact-summary">
<h3>Summary</h3>
<div class="section">
<h4>Artifact Type</h4>
<h4>Material </h4>
<div class="related">
    <h4><?= __('Related Materials') ?></h4>
    <?php if (!empty($artifact->materials)): ?>
    <table cellpadding="0" cellspacing="0">

            <th scope="col"><?= __('Id') ?>
            <th scope="col"><?= __('Material') ?>
            <th scope="col"><?= __('Parent Id') ?>
            <th scope="col" class="actions"><?= __('Actions') ?>

        <?php foreach ($artifact->materials as $materials): ?>

            <?= h($materials->id) ?>
            <?= h($materials->material) ?>
            <?= h($materials->parent_id) ?>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>

<h4>collections</h4>
</div>

<div class="section">
  <h4>Provenience</h4>
  <h4>Period</h4>
  <h4>Genre / Subgenre</h4>
  <h4>languages</h4>
</div>






</div>

<div id="full-detail">
<div class= "detail-leaf" id="translit">
<h4>Transliteration, transcription and translations</h4>
<div class="related">
    <h4><?= __('Related Inscriptions') ?></h4>
    <?php if (!empty($artifact->inscriptions)): ?>
    <table cellpadding="0" cellspacing="0">

            <th scope="col"><?= __('Id') ?>
            <th scope="col"><?= __('Artifact Id') ?>
            <th scope="col"><?= __('Transliteration') ?>
            <th scope="col"><?= __('Transliteration Clean') ?>
            <th scope="col"><?= __('Tranliteration Sign Names') ?>
            <th scope="col"><?= __('User Id') ?>
            <th scope="col"><?= __('Created') ?>
            <th scope="col"><?= __('Credit Id') ?>
            <th scope="col"><?= __('Is Latest') ?>
            <th scope="col"><?= __('Annotation') ?>
            <th scope="col"><?= __('Atf2conll Diff Resolved') ?>
            <th scope="col"><?= __('Atf2conll Diff Unresolved') ?>
            <th scope="col"><?= __('Comments') ?>
            <th scope="col"><?= __('Structure') ?>
            <th scope="col"><?= __('Translations') ?>
            <th scope="col"><?= __('Transcriptions') ?>
            <th scope="col" class="actions"><?= __('Actions') ?>

        <?php foreach ($artifact->inscriptions as $inscriptions): ?>

            <?= h($inscriptions->id) ?>
            <?= h($inscriptions->artifact_id) ?>
            <?= h($inscriptions->transliteration) ?>
            <?= h($inscriptions->transliteration_clean) ?>
            <?= h($inscriptions->tranliteration_sign_names) ?>
            <?= h($inscriptions->user_id) ?>
            <?= h($inscriptions->created) ?>
            <?= h($inscriptions->credit_id) ?>
            <?= h($inscriptions->is_latest) ?>
            <?= h($inscriptions->annotation) ?>
            <?= h($inscriptions->atf2conll_diff_resolved) ?>
            <?= h($inscriptions->atf2conll_diff_unresolved) ?>
            <?= h($inscriptions->comments) ?>
            <?= h($inscriptions->structure) ?>
            <?= h($inscriptions->translations) ?>
            <?= h($inscriptions->transcriptions) ?>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>

</div>
<div class= "detail-leaf" id="annotations">
<h4>Linguistic Annotations</h4>
<div class="related">
    <h4><?= __('Related Inscriptions') ?></h4>
    <?php if (!empty($artifact->inscriptions)): ?>
    <table cellpadding="0" cellspacing="0">

            <th scope="col"><?= __('Id') ?>
            <th scope="col"><?= __('Artifact Id') ?>
            <th scope="col"><?= __('Transliteration') ?>
            <th scope="col"><?= __('Transliteration Clean') ?>
            <th scope="col"><?= __('Tranliteration Sign Names') ?>
            <th scope="col"><?= __('User Id') ?>
            <th scope="col"><?= __('Created') ?>
            <th scope="col"><?= __('Credit Id') ?>
            <th scope="col"><?= __('Is Latest') ?>
            <th scope="col"><?= __('Annotation') ?>
            <th scope="col"><?= __('Atf2conll Diff Resolved') ?>
            <th scope="col"><?= __('Atf2conll Diff Unresolved') ?>
            <th scope="col"><?= __('Comments') ?>
            <th scope="col"><?= __('Structure') ?>
            <th scope="col"><?= __('Translations') ?>
            <th scope="col"><?= __('Transcriptions') ?>
            <th scope="col" class="actions"><?= __('Actions') ?>

        <?php foreach ($artifact->inscriptions as $inscriptions): ?>

            <?= h($inscriptions->id) ?>
            <?= h($inscriptions->artifact_id) ?>
            <?= h($inscriptions->transliteration) ?>
            <?= h($inscriptions->transliteration_clean) ?>
            <?= h($inscriptions->tranliteration_sign_names) ?>
            <?= h($inscriptions->user_id) ?>
            <?= h($inscriptions->created) ?>
            <?= h($inscriptions->credit_id) ?>
            <?= h($inscriptions->is_latest) ?>
            <?= h($inscriptions->annotation) ?>
            <?= h($inscriptions->atf2conll_diff_resolved) ?>
            <?= h($inscriptions->atf2conll_diff_unresolved) ?>
            <?= h($inscriptions->comments) ?>
            <?= h($inscriptions->structure) ?>
            <?= h($inscriptions->translations) ?>
            <?= h($inscriptions->transcriptions) ?>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>

</div>
<div class= "detail-leaf" id="collections">

</div>
<div class= "detail-leaf" id="artifact_meta">
  <h4>Artifact Characteristics</h4>

</div>
<div class= "detail-leaf" id="publications">
  <h4>Publication history</h4>
      <?php if (!empty($artifact->publications)): ?>
sort by publication type first
          <?php foreach ($artifact->publications as $publications): ?>
<li>
              <?= h($publications->bibtexkey) ?>
              author(s)
              <?= h($publications->year) ?>
              <?= h($publications->designation) ?>
              <?= h($publications->entry_type_id) ?>
              <?= h($publications->address) ?>
              <?= h($publications->annote) ?>
              <?= h($publications->book_title) ?>
              <?= h($publications->chapter) ?>
              <?= h($publications->crossref) ?>
              <?= h($publications->edition) ?>
              <?= h($publications->editor) ?>
              <?= h($publications->how_published) ?>
              <?= h($publications->institution) ?>
              <?= h($publications->journal_id) ?>
              <?= h($publications->month) ?>
              <?= h($publications->note) ?>
              <?= h($publications->number) ?>
              <?= h($publications->organization) ?>
              <?= h($publications->pages) ?>
              <?= h($publications->publisher) ?>
              <?= h($publications->school) ?>
              <?= h($publications->title) ?>
              <?= h($publications->volume) ?>
              <?= h($publications->publication_history) ?>
              <?= h($publications->abbreviation_id) ?>
              <?= h($publications->series) ?>
              <?= h($publications->oclc) ?>
              exact reference
              publication type
        </li>

          <?php endforeach; ?>
      <?php endif; ?>
</div>
<div class= "detail-leaf" id="identifyers">
  <h4>Identifyers</h4>
  <?= __('Ark No') ?>
    <?= h($artifact->ark_no) ?>
    <?= __('Composite No') ?>
      <?= h($artifact->composite_no) ?>


</div>
<div class= "detail-leaf" id="arch">
  <h4>Archaeological Information</h4>
  <?= __('Condition Description') ?>
    <?= h($artifact->condition_description) ?>




  <?= __('Elevation') ?>
    <?= h($artifact->elevation) ?>


  <?= __('Excavation No') ?>
    <?= h($artifact->excavation_no) ?>


  <?= __('Findspot Square') ?>
    <?= h($artifact->findspot_square) ?>


  <?= __('Join Information') ?>
    <?= h($artifact->join_information) ?>

    <?= __('Artifact Preservation') ?>
      <?= h($artifact->artifact_preservation) ?>



</div>

<div class= "detail-leaf" id="credits">
  <h4>Credits</h4>

    <?= __('Cdli Collation') ?>
      <?= h($artifact->cdli_collation) ?>
      <?= __('Created') ?>
        <?= h($artifact->created) ?>


      <?= __('Date Comments') ?>
        <?= h($artifact->date_comments) ?>


      <?= __('Modified') ?>
        <?= h($artifact->modified) ?>



</div>

<div class= "detail-leaf" id="externals">
    <h4>External Resources</h4>
    <?php if (!empty($artifact->external_resources)): ?>
        <?php foreach ($artifact->external_resources as $externalResources): ?>
<li>
          <?= h($externalResources->abbrev) ?>  <a href=" <?= h($externalResources->base_url) ?>/external_resource_key">
</li>
            <?php endforeach; ?>
        <?php endif; ?>


</div>


</div>




  <?= __('Primary Publication Comments') ?>
    <?= h($artifact->primary_publication_comments) ?>








          <?= __('Museum No') ?>
            <?= h($artifact->museum_no) ?>




          <?= __('Seal No') ?>
            <?= h($artifact->seal_no) ?>


          <?= __('Stratigraphic Level') ?>
            <?= h($artifact->stratigraphic_level) ?>


          <?= __('Surface Preservation') ?>
            <?= h($artifact->surface_preservation) ?>


          <?= __('Provenience') ?>
            <?= $artifact->has('provenience') ? $this->Html->link($artifact->provenience->id, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) : '' ?>


          <?= __('Period') ?>
            <?= $artifact->has('period') ? $this->Html->link($artifact->period->id, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) : '' ?>


          <?= __('Artifact Type') ?>
            <?= $artifact->has('artifact_type') ? $this->Html->link($artifact->artifact_type->id, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) : '' ?>


          <?= __('Archive') ?>
            <?= $artifact->has('archive') ? $this->Html->link($artifact->archive->id, ['controller' => 'Archives', 'action' => 'view', $artifact->archive->id]) : '' ?>


          <?= __('Created By') ?>
            <?= h($artifact->created_by) ?>


          <?= __('Id') ?>
            <?= $this->Number->format($artifact->id) ?>


          <?= __('Credit Id') ?>
            <?= $this->Number->format($artifact->credit_id) ?>


          <?= __('Height') ?>
            <?= $this->Number->format($artifact->height) ?>


          <?= __('Thickness') ?>
            <?= $this->Number->format($artifact->thickness) ?>


          <?= __('Width') ?>
            <?= $this->Number->format($artifact->width) ?>


          <?= __('Accounting Period') ?>
            <?= $this->Number->format($artifact->accounting_period) ?>


          <?= __('Written In') ?>
            <?= $this->Number->format($artifact->written_in) ?>


          <?= __('Is Object Type Uncertain') ?>
            <?= $this->Number->format($artifact->is_object_type_uncertain) ?>


          <?= __('Weight') ?>
            <?= $this->Number->format($artifact->weight) ?>


          <?= __('Dates Referenced') ?>
            <?= h($artifact->dates_referenced) ?>


          <?= __('Is Public') ?>
            <?= $artifact->is_public ? __('Yes') : __('No'); ?>


          <?= __('Is Atf Public') ?>
            <?= $artifact->is_atf_public ? __('Yes') : __('No'); ?>


          <?= __('Are Images Public') ?>
            <?= $artifact->are_images_public ? __('Yes') : __('No'); ?>


          <?= __('Is Provenience Uncertain') ?>
            <?= $artifact->is_provenience_uncertain ? __('Yes') : __('No'); ?>


          <?= __('Is Period Uncertain') ?>
            <?= $artifact->is_period_uncertain ? __('Yes') : __('No'); ?>


          <?= __('Is School Text') ?>
            <?= $artifact->is_school_text ? __('Yes') : __('No'); ?>

    </table>
    <div class="row">
        <h4><?= __('Cdli Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->cdli_comments)); ?>
    </div>
    <div class="row">
        <h4><?= __('Findspot Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->findspot_comments)); ?>
    </div>
    <div class="row">
        <h4><?= __('Seal Information') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->seal_information)); ?>
    </div>
    <div class="row">
        <h4><?= __('General Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->general_comments)); ?>
    </div>
    <div class="row">
        <h4><?= __('Accession No') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->accession_no)); ?>
    </div>
    <div class="row">
        <h4><?= __('Alternative Years') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->alternative_years)); ?>
    </div>
    <div class="row">
        <h4><?= __('Dumb2') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->dumb2)); ?>
    </div>
    <div class="row">
        <h4><?= __('Custom Designation') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->custom_designation)); ?>
    </div>
    <div class="row">
        <h4><?= __('Period Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->period_comments)); ?>
    </div>
    <div class="row">
        <h4><?= __('Provenience Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->provenience_comments)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Credits') ?></h4>
        <?php if (!empty($artifact->credits)): ?>
        <table cellpadding="0" cellspacing="0">

                <th scope="col"><?= __('Id') ?>
                <th scope="col"><?= __('User Id') ?>
                <th scope="col"><?= __('Artifact Id') ?>
                <th scope="col"><?= __('Date') ?>
                <th scope="col"><?= __('Comments') ?>
                <th scope="col" class="actions"><?= __('Actions') ?>

            <?php foreach ($artifact->credits as $credits): ?>

                <?= h($credits->id) ?>
                <?= h($credits->user_id) ?>
                <?= h($credits->artifact_id) ?>
                <?= h($credits->date) ?>
                <?= h($credits->comments) ?>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Credits', 'action' => 'view', $credits->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Credits', 'action' => 'edit', $credits->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Credits', 'action' => 'delete', $credits->id], ['confirm' => __('Are you sure you want to delete # {0}?', $credits->id)]) ?>


            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Collections') ?></h4>
        <?php if (!empty($artifact->collections)): ?>
        <table cellpadding="0" cellspacing="0">

                <th scope="col"><?= __('Id') ?>
                <th scope="col"><?= __('Collection') ?>
                <th scope="col"><?= __('Geo Coordinates') ?>
                <th scope="col"><?= __('Slug') ?>
                <th scope="col"><?= __('Is Private') ?>
                <th scope="col" class="actions"><?= __('Actions') ?>

            <?php foreach ($artifact->collections as $collections): ?>

                <?= h($collections->id) ?>
                <?= h($collections->collection) ?>
                <?= h($collections->geo_coordinates) ?>
                <?= h($collections->slug) ?>
                <?= h($collections->is_private) ?>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Collections', 'action' => 'view', $collections->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Collections', 'action' => 'edit', $collections->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Collections', 'action' => 'delete', $collections->id], ['confirm' => __('Are you sure you want to delete # {0}?', $collections->id)]) ?>


            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>



    <div class="related">
        <h4><?= __('Related Genres') ?></h4>
        <?php if (!empty($artifact->genres)): ?>
        <table cellpadding="0" cellspacing="0">

                <th scope="col"><?= __('Id') ?>
                <th scope="col"><?= __('Genre') ?>
                <th scope="col"><?= __('Parent Id') ?>
                <th scope="col"><?= __('Genre Comments') ?>
                <th scope="col" class="actions"><?= __('Actions') ?>

            <?php foreach ($artifact->genres as $genres): ?>

                <?= h($genres->id) ?>
                <?= h($genres->genre) ?>
                <?= h($genres->parent_id) ?>
                <?= h($genres->genre_comments) ?>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Genres', 'action' => 'view', $genres->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Genres', 'action' => 'edit', $genres->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Genres', 'action' => 'delete', $genres->id], ['confirm' => __('Are you sure you want to delete # {0}?', $genres->id)]) ?>


            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Languages') ?></h4>
        <?php if (!empty($artifact->languages)): ?>
        <table cellpadding="0" cellspacing="0">

                <th scope="col"><?= __('Id') ?>
                <th scope="col"><?= __('Order') ?>
                <th scope="col"><?= __('Parent Id') ?>
                <th scope="col"><?= __('Language') ?>
                <th scope="col"><?= __('Protocol Code') ?>
                <th scope="col"><?= __('Inline Code') ?>
                <th scope="col"><?= __('Notes') ?>
                <th scope="col" class="actions"><?= __('Actions') ?>

            <?php foreach ($artifact->languages as $languages): ?>

                <?= h($languages->id) ?>
                <?= h($languages->order) ?>
                <?= h($languages->parent_id) ?>
                <?= h($languages->language) ?>
                <?= h($languages->protocol_code) ?>
                <?= h($languages->inline_code) ?>
                <?= h($languages->notes) ?>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Languages', 'action' => 'view', $languages->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Languages', 'action' => 'edit', $languages->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Languages', 'action' => 'delete', $languages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $languages->id)]) ?>


            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>

    <div class="related">
        <h4><?= __('Related Artifacts Composites') ?></h4>
        <?php if (!empty($artifact->artifacts_composites)): ?>
        <table cellpadding="0" cellspacing="0">

                <th scope="col"><?= __('Id') ?>
                <th scope="col"><?= __('Composite') ?>
                <th scope="col"><?= __('Artifact Id') ?>
                <th scope="col" class="actions"><?= __('Actions') ?>

            <?php foreach ($artifact->artifacts_composites as $artifactsComposites): ?>

                <?= h($artifactsComposites->id) ?>
                <?= h($artifactsComposites->composite) ?>
                <?= h($artifactsComposites->artifact_id) ?>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ArtifactsComposites', 'action' => 'view', $artifactsComposites->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ArtifactsComposites', 'action' => 'edit', $artifactsComposites->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ArtifactsComposites', 'action' => 'delete', $artifactsComposites->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsComposites->id)]) ?>


            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Artifacts Date Referenced') ?></h4>
        <?php if (!empty($artifact->artifacts_date_referenced)): ?>
        <table cellpadding="0" cellspacing="0">

                <th scope="col"><?= __('Id') ?>
                <th scope="col"><?= __('Artifact Id') ?>
                <th scope="col"><?= __('Ruler Id') ?>
                <th scope="col"><?= __('Month Id') ?>
                <th scope="col"><?= __('Month No') ?>
                <th scope="col"><?= __('Year Id') ?>
                <th scope="col"><?= __('Day No') ?>
                <th scope="col" class="actions"><?= __('Actions') ?>

            <?php foreach ($artifact->artifacts_date_referenced as $artifactsDateReferenced): ?>

                <?= h($artifactsDateReferenced->id) ?>
                <?= h($artifactsDateReferenced->artifact_id) ?>
                <?= h($artifactsDateReferenced->ruler_id) ?>
                <?= h($artifactsDateReferenced->month_id) ?>
                <?= h($artifactsDateReferenced->month_no) ?>
                <?= h($artifactsDateReferenced->year_id) ?>
                <?= h($artifactsDateReferenced->day_no) ?>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ArtifactsDateReferenced', 'action' => 'view', $artifactsDateReferenced->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ArtifactsDateReferenced', 'action' => 'edit', $artifactsDateReferenced->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ArtifactsDateReferenced', 'action' => 'delete', $artifactsDateReferenced->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsDateReferenced->id)]) ?>


            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Artifacts Seals') ?></h4>
        <?php if (!empty($artifact->artifacts_seals)): ?>
        <table cellpadding="0" cellspacing="0">

                <th scope="col"><?= __('Id') ?>
                <th scope="col"><?= __('Seal No') ?>
                <th scope="col"><?= __('Artifact Id') ?>
                <th scope="col" class="actions"><?= __('Actions') ?>

            <?php foreach ($artifact->artifacts_seals as $artifactsSeals): ?>

                <?= h($artifactsSeals->id) ?>
                <?= h($artifactsSeals->seal_no) ?>
                <?= h($artifactsSeals->artifact_id) ?>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ArtifactsSeals', 'action' => 'view', $artifactsSeals->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ArtifactsSeals', 'action' => 'edit', $artifactsSeals->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ArtifactsSeals', 'action' => 'delete', $artifactsSeals->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsSeals->id)]) ?>


            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Artifacts Shadow') ?></h4>
        <?php if (!empty($artifact->artifacts_shadow)): ?>
        <table cellpadding="0" cellspacing="0">

                <th scope="col"><?= __('Id') ?>
                <th scope="col"><?= __('Artifact Id') ?>
                <th scope="col"><?= __('Cdli Comments') ?>
                <th scope="col"><?= __('Collection Location') ?>
                <th scope="col"><?= __('Collection Comments') ?>
                <th scope="col"><?= __('Acquisition History') ?>
                <th scope="col" class="actions"><?= __('Actions') ?>

            <?php foreach ($artifact->artifacts_shadow as $artifactsShadow): ?>

                <?= h($artifactsShadow->id) ?>
                <?= h($artifactsShadow->artifact_id) ?>
                <?= h($artifactsShadow->cdli_comments) ?>
                <?= h($artifactsShadow->collection_location) ?>
                <?= h($artifactsShadow->collection_comments) ?>
                <?= h($artifactsShadow->acquisition_history) ?>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ArtifactsShadow', 'action' => 'view', $artifactsShadow->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ArtifactsShadow', 'action' => 'edit', $artifactsShadow->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ArtifactsShadow', 'action' => 'delete', $artifactsShadow->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsShadow->id)]) ?>


            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>

    <div class="related">
        <h4><?= __('Related Retired Artifacts') ?></h4>
        <?php if (!empty($artifact->retired_artifacts)): ?>
        <table cellpadding="0" cellspacing="0">

                <th scope="col"><?= __('Id') ?>
                <th scope="col"><?= __('Artifact Id') ?>
                <th scope="col"><?= __('New Artifact Id') ?>
                <th scope="col"><?= __('Artifact Remarks') ?>
                <th scope="col"><?= __('Is Public') ?>
                <th scope="col" class="actions"><?= __('Actions') ?>

            <?php foreach ($artifact->retired_artifacts as $retiredArtifacts): ?>

                <?= h($retiredArtifacts->id) ?>
                <?= h($retiredArtifacts->artifact_id) ?>
                <?= h($retiredArtifacts->new_artifact_id) ?>
                <?= h($retiredArtifacts->artifact_remarks) ?>
                <?= h($retiredArtifacts->is_public) ?>

            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArtifactsExternalResourcesFixture
 *
 */
class ArtifactsExternalResourcesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'artifact_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'external_resource_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'external_resource_key' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'fk_artifacts_idx_6' => ['type' => 'index', 'columns' => ['artifact_id'], 'length' => []],
            'fk_external_resources_idx' => ['type' => 'index', 'columns' => ['external_resource_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_artifacts_6' => ['type' => 'foreign', 'columns' => ['artifact_id'], 'references' => ['artifacts', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_external_resources' => ['type' => 'foreign', 'columns' => ['external_resource_id'], 'references' => ['external_resources', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'artifact_id' => 1,
                'external_resource_id' => 1,
                'external_resource_key' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
